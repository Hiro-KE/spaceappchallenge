// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpaceAppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPACEAPP_API ASpaceAppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
